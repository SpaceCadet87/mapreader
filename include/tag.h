#pragma once

#include <vector>
#include <string>

namespace TAG {
    using std::vector;
    enum types{
        TAG_End,
        TAG_Byte,
        TAG_Short,
        TAG_Int,
        TAG_Long,
        TAG_Float,
        TAG_Double,
        TAG_Byte_Array,
        TAG_String,
        TAG_List,
        TAG_Compound,
        TAG_Int_Array,
        TAG_Long_Array,
        TAG_NULL
    };
    struct description {
        types type_id;
        std::string name;
    };
    struct tag_parent {
        types type_id;
        std::string name;
        types content_type;
        int32_t remaining = 0;
    };
    class TAG
    {
    private:
        std::string convert_string(char *d);
        void generate_index();
    public:
        types type_id = TAG_NULL;
        std::string name;
        types content_type;
        int32_t entries = 0;
        char *content;
        char *next;
        int8_t value_byte();
        int32_t value_int();
        int64_t value_long();
        std::string value_string();
        TAG find_end();
        TAG(char *d, types t, std::string n);
        TAG(char *d);
        TAG();
        ~TAG();
        vector<TAG> find(vector<description>);
        TAG find(description);
        TAG operator [](int32_t i);
    };
}