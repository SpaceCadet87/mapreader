#pragma once
#include <compare.h>

void merge_regions(Region::Region *source, std::vector<Compare::chunk_mask> *mask, Region::Region *dest);