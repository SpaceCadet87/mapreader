#pragma once

#include <filesystem>
#include <chunk.h>

namespace Region {
    class Region
    {
    public:
        Region(std::filesystem::path);
        ~Region();
        Chunk::Chunk *at_location(int32_t x, int32_t z);
        std::vector<Chunk::Chunk> chunks;
    };
}