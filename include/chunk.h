#pragma once

#include <ostream>
#include <cstdint>
#include <tag.h>

namespace Chunk{
    struct Timestamp {
        uint32_t data;
    };
    class Location {
        private:
            uint32_t value;
        public:
            uint32_t size();
            uint64_t offset();
    };
    enum CompressionScheme{
        gzip = 1,
        zlib = 2
    };
    struct Length{
        public:
            uint32_t data;
            operator size_t();
    };
    struct Header{
        Length length;
        CompressionScheme compression;
    };
    class Chunk
    {
        private:
            std::vector<char> decompress(char* input, size_t size);
            char * data;
        public:
            Timestamp timestamp;
            TAG::TAG root;
            std::string status;
            int32_t x_pos;
            int32_t z_pos;
            TAG::TAG sections;
            Chunk(char* compressed, size_t size, Timestamp header_timestamp);
            Chunk(Chunk &&source);
            ~Chunk();
    };
}

std::ostream &operator<<(std::ostream &os, Chunk::Timestamp const &m);
std::ostream &operator<<(std::ostream &os, Chunk::Length const &m);