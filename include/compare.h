#pragma once
#include <region.h>
#include <bitset>

namespace Compare {
    struct section_mask
    {
        bool dirty = false;
        std::bitset<16> mask[256];
    };

    struct chunk_mask
    {
        int32_t xPos;
        int32_t yPos;
        int32_t zPos;
        std::vector<section_mask> sectionMask;
    };

    std::vector<chunk_mask> generate_region_mask(Region::Region *base, Region::Region *comparison);
}