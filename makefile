DIR_SRC=src
DIR_INC=include
DIR_BUILD=build
DIR_OBJ=$(DIR_BUILD)/obj

sources := $(notdir $(wildcard $(DIR_SRC)/*.cpp))
obj := $(addprefix $(DIR_OBJ)/,$(patsubst %.cpp,%.o,$(sources)))
BUILD_FLAGS := -lz $(addprefix -I ,$(DIR_INC))
DEBUG_FLAGS := -DDEBUG -g

all : $(DIR_BUILD)/mapreader

debug : BUILD_FLAGS += $(DEBUG_FLAGS)
debug : $(DIR_BUILD)/mapreader

clean :
	@rm -r $(DIR_BUILD)

$(DIR_BUILD)/mapreader : $(obj) mapreader.cpp
	$(CXX) $(BUILD_FLAGS) $^ -o $@

$(DIR_OBJ)/%.o : $(DIR_SRC)/%.cpp $(DIR_INC)/%.h
	@mkdir -p $(@D)
	$(CXX) $(BUILD_FLAGS) -c $< -o $@