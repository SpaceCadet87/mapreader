#include <compare.h>
#include <merge.h>
#include <iostream>

// https://wiki.vg/Region_Files
// https://wiki.vg/NBT
// https://wiki.vg/Chunk_Format
// https://minecraft.fandom.com/wiki/Chunk_format#NBT_structure

int main(int argc, char *argv[]){
    using std::filesystem::path;
    if(argc < 4) return -1;
    Region::Region base(argv[1]);
    Region::Region source(argv[2]);
    Region::Region host(argv[3]);
    {
        using namespace Compare;
        std::cout << "Generating mask..." << std::endl;
        std::vector<chunk_mask> mask = generate_region_mask(&base, &source);
        std::cout << "Merging regions..." << std::endl;
        merge_regions(&source, &mask, &host);
        std::cout << "Done" << std::endl;
        std::cout << std::flush;
    }
}