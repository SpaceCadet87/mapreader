#include <tag.h>
#include <cstring>

namespace TAG{
    using std::vector;

    std::string tag_names[]{
        "TAG_End",
        "TAG_Byte",
        "TAG_Short",
        "TAG_Int",
        "TAG_Long",
        "TAG_Float",
        "TAG_Double",
        "TAG_Byte_Array",
        "TAG_String",
        "TAG_List",
        "TAG_Compound",
        "TAG_Int_Array",
        "TAG_Long_Array"
    };

    std::string TAG::convert_string(char *d){
        size_t length = __builtin_bswap16(*(uint16_t*)(d));
        if(length == 0) return std::string();
        char buf[length + 1];
        memcpy(buf, (char*)(&d[2]), length);
        buf[length] = '\0';
        return std::string(buf);
    }

    void TAG::generate_index(){
        next = content;
        switch (type_id)
        {
            case TAG_Byte:
                next += 1;
                break;
            case TAG_Short:
                next += 2;
                break;
            case TAG_Int:
                next += 4;
                break;
            case TAG_Long:
                next += 8;
                break;
            case TAG_Float:
                next += 4;
                break;
            case TAG_Double:
                next += 8;
                break;
            case TAG_Byte_Array:{
                entries = (int32_t)__builtin_bswap32(*(int32_t*)content);
                if(entries > 0){
                    next += entries + 4;
                }}
                break;
            case TAG_String:
                next += convert_string(content).length() + 2;
                break;
            case TAG_List:
                content_type = (types)*content;
                entries = (int32_t)__builtin_bswap32(*(int32_t*)&content[1]);
                next += 5;
                break;
            case TAG_Compound:
                break;
            case TAG_Int_Array:{
                entries = (int32_t)__builtin_bswap32(*(int32_t*)content);
                if(entries > 0){
                    next += (entries * 4) + 4;
                }}
                break;
            case TAG_Long_Array:{
                entries = (int32_t)__builtin_bswap32(*(int32_t*)content);
                if(entries > 0){
                    next += (entries * 8) + 4;
                }}
                break;
        default:
            break;
        }
    }

    TAG TAG::find_end(){
        TAG retVal;
        char *_next = next;
        vector<tag_parent> tag_stack({{
            type_id,
            name
        }});
        while(tag_stack.size() > 0){
            if(tag_stack.back().type_id == TAG_Compound){
                retVal = TAG(_next);
                _next = retVal.next;
            }
            else if(tag_stack.back().type_id == TAG_List){
                retVal = TAG(_next, tag_stack.back().content_type, "");
                _next = retVal.next;
            }
            if(tag_stack.back().type_id == TAG_List){
                if(tag_stack.back().remaining > 0) tag_stack.back().remaining--;
                if(tag_stack.back().remaining == 0 && retVal.type_id != TAG_Compound){
                    tag_stack.pop_back();
                }
            }
            switch (retVal.type_id)
            {
            case TAG_List:
                if(retVal.entries > 0){
                    tag_stack.push_back({retVal.type_id, retVal.name, retVal.content_type, retVal.entries});
                }
                break;
            case TAG_Compound:
                    tag_stack.push_back({retVal.type_id, retVal.name});
                break;
            case TAG_End:
                tag_stack.pop_back();
                if(tag_stack.size() > 0 && tag_stack.back().type_id == TAG_List && tag_stack.back().remaining == 0){
                    tag_stack.pop_back();
                }
                break;
            }
        }
        return retVal;
    }

    int8_t TAG::value_byte(){
        if(type_id != TAG_Byte) return 0;
        return (int8_t)*content;
    }

    int32_t TAG::value_int(){
        if(type_id != TAG_Int) return 0;
        return (int32_t)__builtin_bswap32(*(int32_t*)content);
    }

    int64_t TAG::value_long(){
        if(type_id != TAG_Long) return 0;
        return (int64_t)__builtin_bswap64(*(int64_t*)content);
    }

    std::string TAG::value_string(){
        if(type_id != TAG_String) return "";
        return convert_string(content);
    }

    TAG::TAG(){}

    TAG::TAG(char *d, types t, std::string n){
        type_id = t;
        name = n;
        content = d;
        generate_index();
    }

    TAG::TAG(char *d)
    {
        type_id = (types)*d;
        if(type_id == TAG_End){
            name = "";
            content = d;
            next = content + 1;
            return;
        }
        name = convert_string(&d[1]);
        content = d + name.size() + 3;
        content_type = TAG_End;
        generate_index();
    }
    
    TAG::~TAG()
    {
    }

    vector<TAG> TAG::find(vector<description> q){
        vector<TAG> retVal{q.size()};
        char *_next = next;
        vector<tag_parent> tag_stack({{
            type_id,
            name
        }});
        while(tag_stack.size() > 0){
            TAG this_tag;
            if(tag_stack.back().type_id == TAG_Compound){
                this_tag = TAG(_next);
                _next = this_tag.next;
            }
            else if(tag_stack.back().type_id == TAG_List){
                this_tag = TAG(_next, tag_stack.back().content_type, "");
                _next = this_tag.next;
            }
            if(tag_stack.back().type_id == TAG_List){
                if(tag_stack.back().remaining > 0) tag_stack.back().remaining--;
                if(tag_stack.back().remaining == 0 && this_tag.type_id != TAG_Compound){
                    tag_stack.pop_back();
                }
            }
            for(size_t i = 0; i < q.size(); i++){
                if(
                    retVal[i].type_id == this_tag.type_id &&
                    retVal[i].name == this_tag.name
                ) break;
                if(
                    q[i].type_id == this_tag.type_id &&
                    q[i].name == this_tag.name
                )
                {
                    retVal[i] = this_tag;
                    break;
                }
            }
            switch (this_tag.type_id)
            {
            case TAG_List:
                if(this_tag.entries > 0){
                    tag_stack.push_back({this_tag.type_id, this_tag.name, this_tag.content_type, this_tag.entries});
                }
                break;
            case TAG_Compound:
                    tag_stack.push_back({this_tag.type_id, this_tag.name});
                break;
            case TAG_End:
                tag_stack.pop_back();
                if(tag_stack.size() > 0 && tag_stack.back().type_id == TAG_List && tag_stack.back().remaining == 0){
                    tag_stack.pop_back();
                }
                break;
            }
        }
        return retVal;
    }

    TAG TAG::find(description q){
        return find((vector<description>){q})[0];
    }

    TAG TAG::operator[] (int32_t index) {
        TAG retVal;
        char *_next = next;
        if(type_id == TAG_List && content_type == TAG_Compound){
            for(int32_t i = 0; i <= index; i++){
                retVal = TAG(_next, content_type, "");
                _next = retVal.find_end().next;
            }
        }
        else if(type_id == TAG_Long_Array){
            retVal = TAG(content+(index*8), TAG_Long, "");
        }
        return retVal;
    };
}