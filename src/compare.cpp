#include <compare.h>
#include <vector>
#include <map>

class BlockData
{
public:
    BlockData(TAG::TAG block_states){
        std::vector<TAG::TAG> src = block_states.find({{TAG::TAG_Long_Array, "data"},{TAG::TAG_List, "palette"}});
        palette = src[1];
        data = src[0];
    }
    TAG::TAG palette;
    TAG::TAG data;
};

struct palette_entry
{
    std::string name;
    std::map<std::string, std::string> properties;
};

typedef std::vector<palette_entry> BlockPalette;

BlockPalette get_palette_from_tag(TAG::TAG palette){
    std::vector<palette_entry> retVal;
    for(int32_t i = 0; i < palette.entries; i++){
        std::vector<TAG::TAG> src = palette[i].find({{TAG::TAG_String, "Name"},{TAG::TAG_Compound, "Properties"}});
        retVal.push_back({name: src[0].value_string()});
        if(src[1].type_id == TAG::TAG_Compound){
            char *next = src[1].next;
            TAG::TAG this_tag = TAG::TAG(next);
            while(this_tag.type_id == TAG::TAG_String){
                retVal.back().properties[this_tag.name] = this_tag.value_string();
                next = this_tag.next;
                this_tag = TAG::TAG(next);
            }
        }
    }
    return retVal;
}

struct palette_mapping
{
    BlockPalette palette;
    std::vector<int32_t> base;
    std::vector<int32_t> comparison;
};

palette_mapping generate_palette_mapping(BlockData base, BlockData comparison){
    palette_mapping retVal;
    BlockPalette base_palette = get_palette_from_tag(base.palette);
    BlockPalette comparison_palette = get_palette_from_tag(comparison.palette);
    retVal.palette.insert(retVal.palette.end(), base_palette.begin(), base_palette.end());
    for(int32_t i = 0; i < retVal.palette.size(); i++){
        retVal.base.push_back(i);
    }
    for (int32_t i0 = 0; i0 < comparison_palette.size(); i0++)
    {
        bool exists = false;
        for (int32_t i1 = 0; i1 < retVal.palette.size(); i1++){
            if(
                comparison_palette[i0].name == retVal.palette[i1].name &&
                comparison_palette[i0].properties.size() == retVal.palette[i1].properties.size() &&
                std::equal(
                    comparison_palette[i0].properties.begin(),
                    comparison_palette[i0].properties.end(),
                    retVal.palette[i1].properties.begin()
                )
            )
            {
                exists = true;
                if(retVal.comparison.size() < i0+1) retVal.comparison.resize(i0+1);
                retVal.comparison[i0] = i1;
                break;
            }
        }
        if(!exists){
            if(retVal.comparison.size() < i0+1) retVal.comparison.resize(i0+1);
            retVal.comparison[i0] = retVal.palette.size();
            retVal.palette.push_back(comparison_palette[i0]);
        }
    }
    return retVal;
}

Compare::section_mask generate_section_mask(BlockData base, BlockData comparison){
    using namespace Compare;
    section_mask retVal;
    palette_mapping p_map = generate_palette_mapping(base, comparison);
    for(int32_t i0 = 0; i0 < 256; i0++){
        int64_t base_block = base.data[i0].value_long();
        int64_t compare_block = comparison.data[i0].value_long();
        for(size_t i1 = 0; i1 < 64; i1 += 4){
            int base_val = p_map.base[(0b1111 & (base_block >> 64-(i1+4)))];
            int compare_val = p_map.comparison[(0b1111 & (compare_block >> 64-(i1+4)))];
            if(base_val != compare_val){
                retVal.mask[i0].set(i1 >> 2);
                retVal.dirty = true;
            }
        }
    }
    return retVal;
}

Compare::chunk_mask generate_chunk_mask(Chunk::Chunk *base, Chunk::Chunk *comparison){
    using namespace Compare;
    chunk_mask retVal = {
        xPos: comparison->x_pos,
        zPos: comparison->z_pos
    };
    int32_t search_start = 0;
    for(int32_t i0 = 0; i0 < comparison->sections.entries; i0++){
        for(int32_t i1 = search_start; i1 < base->sections.entries; i1++){
            std::vector<TAG::TAG> src = base->sections[i1].find({{TAG::TAG_Compound, "block_states"}, {TAG::TAG_Byte, "Y"}});
            BlockData base_data = BlockData(src[0]);
            int8_t base_y = src[1].value_byte();
            src = comparison->sections[i0].find({{TAG::TAG_Compound, "block_states"}, {TAG::TAG_Byte, "Y"}});
            BlockData comparison_data = BlockData(src[0]);
            int8_t comparison_y = src[1].value_byte();
            if(base_y == comparison_y){
                if(i1 == search_start) search_start++;
                retVal.sectionMask.push_back(generate_section_mask(base_data, comparison_data));
                retVal.yPos = comparison_y;
                break;
            }
        }
    }
    return retVal;
}

namespace Compare {
    std::vector<chunk_mask> generate_region_mask(Region::Region *base, Region::Region *comparison){
        std::vector<chunk_mask> region_mask;
        for(size_t i0 = 0; i0 < comparison->chunks.size(); i0++){
            if(comparison->chunks[i0].status != "full") continue;
            for(size_t i1 = 0; i1 < base->chunks.size(); i1++){
                if(
                    comparison->chunks[i0].x_pos == base->chunks[i1].x_pos &&
                    comparison->chunks[i0].z_pos == base->chunks[i1].z_pos
                ){
                    if(base->chunks[i1].status != "full") continue;
                    region_mask.push_back(generate_chunk_mask(&base->chunks[i1], &comparison->chunks[i0]));
                }
            }
        }
        return region_mask;
    }
}
