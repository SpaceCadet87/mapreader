#include <iostream>
#include <merge.h>

void merge_blocks(Compare::section_mask *mask){
    for(uint8_t i = 0; i < 255; i++){
        std::bitset<16> these_blocks = mask->mask[i];
    }
    std::cout << std::flush;
}

void merge_sections(Chunk::Chunk *source, Chunk::Chunk *dest, std::vector<Compare::section_mask> *mask){
    for(size_t i = 0; i < mask->size(); i++){
        Compare::section_mask *this_section = &(mask->at(i));
        if(!this_section->dirty) continue;
        std::cout << "Section #" << i << std::endl;
        merge_blocks(this_section);
    }
}

void merge_chunks(Region::Region *source, Region::Region *dest, Compare::chunk_mask *mask){
    int32_t xPos = mask->xPos;
    int32_t zPos = mask->zPos;
    std::cout << "X:" << xPos << " Y:" << zPos << std::endl;
    Chunk::Chunk *source_chunk = source->at_location(xPos, zPos);
    Chunk::Chunk *dest_chunk = dest->at_location(xPos, zPos);
    merge_sections(source_chunk, dest_chunk, &mask->sectionMask);
}

bool section_is_dirty(Compare::chunk_mask *mask){
    for(size_t i = 0; i < mask->sectionMask.size(); i++){
        if(mask->sectionMask[i].dirty == true) return true;
    }
    return false;
}

void merge_regions(Region::Region *source, std::vector<Compare::chunk_mask> *mask, Region::Region *dest){
    for(size_t i = 0; i < mask->size(); i++){
        Compare::chunk_mask *this_chunk_mask = &mask->at(i);
        if(!section_is_dirty(this_chunk_mask)) continue;
        merge_chunks(source, dest, this_chunk_mask);
        // for(size_t i = 0; i < source->chunks.size(); i++){
        //     Chunk::Chunk *source_chunk = &source->chunks[i];
        //     Chunk::Chunk *dest_chunk = &source->chunks[i];
        // }
    }
}