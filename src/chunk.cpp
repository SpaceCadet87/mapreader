#include <chunk.h>

#include <zlib.h>

#define table_value_scaling 4096

namespace Chunk{
    std::vector<char> Chunk::decompress(char* input, size_t size){
        std::vector<char> ret_val;
        char buf[65536];
        z_stream infstream;
        infstream.zalloc = Z_NULL;
        infstream.zfree = Z_NULL;
        infstream.opaque = Z_NULL;
        infstream.avail_in = (uInt)(size-1); // size of input
        infstream.next_in = (Bytef *)input; // input char array

        inflateInit(&infstream);
        size_t running = 0;
        while (infstream.avail_in)
        {
            infstream.avail_out = (uInt)sizeof(buf); // size of output
            infstream.next_out = (Bytef *)buf; // output char array
            inflate(&infstream, Z_NO_FLUSH);
            ret_val.insert(ret_val.end(), buf, buf+(infstream.total_out - running));
            running = infstream.total_out;
        }
        inflateEnd(&infstream);
        return ret_val;
    }

    Chunk::Chunk(char* compressed, size_t size, Timestamp header_timestamp)
    {
        timestamp = header_timestamp;
        std::vector<char> _data = decompress(compressed, size);
        data = new char[_data.size()];
        std::copy(_data.begin(), _data.end(), data);
        root = TAG::TAG(data);
        std::vector<TAG::TAG> details = root.find({
            {
                type_id: TAG::TAG_Int,
                name: "xPos"
            },
            {
                type_id: TAG::TAG_Int,
                name: "zPos"
            },
            {
                type_id: TAG::TAG_String,
                name: "Status"
            },
            {
                type_id: TAG::TAG_List,
                name: "sections"
            }
        });
        x_pos = details[0].value_int();
        z_pos = details[1].value_int();
        status = details[2].value_string();
        sections = details[3];
    }

    Chunk::Chunk(Chunk &&source){
        timestamp = source.timestamp;
        root = source.root;
        status = source.status;
        x_pos = source.x_pos;
        z_pos = source.z_pos;
        sections = source.sections;
        data = source.data;
        source.data = NULL;
    }

    Chunk::~Chunk()
    {
        delete[] data;
    }

    uint32_t Location::size(){
        return (value >> 24) * table_value_scaling;
    }

    uint64_t Location::offset(){
        return (((value & 0xFF) << 16) | (value & 0xFF00) | ((value & 0xFF0000) >> 16)) * table_value_scaling;
    }
}

std::ostream &operator<<(std::ostream &os, Chunk::Timestamp const &m) {
    return os << __builtin_bswap32(m.data);
}

Chunk::Length::operator size_t(){
    return __builtin_bswap32(this->data);
}

std::ostream &operator<<(std::ostream &os, Chunk::Length const &m) {
    return os << __builtin_bswap32(m.data);
}