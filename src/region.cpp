#include <region.h>
#include <fstream>

using std::filesystem::path;

namespace Region {
    Region::Region(path filename)
    {
        std::ifstream in_file (filename, std::ios::in | std::ios::binary);
        if (!in_file.is_open()) return;
        Chunk::Location location_table[1024];
        Chunk::Timestamp timestamp_table[1024];
        in_file.read(reinterpret_cast<char*>(&location_table), sizeof(location_table));
        in_file.read(reinterpret_cast<char*>(&timestamp_table), sizeof(timestamp_table));
        for(size_t i = 0; i < 1024; i++){
            if(location_table[i].size() == 0) continue;
            in_file.seekg(location_table[i].offset());
            Chunk::Header this_header;
            in_file.read(reinterpret_cast<char*>(&this_header), 5);
            char *compressed_data = new char[this_header.length];
            in_file.read(compressed_data, this_header.length);
            chunks.emplace_back(compressed_data, this_header.length, timestamp_table[i]);
        }
        in_file.close();
    }

    Region::~Region()
    {
    }

    Chunk::Chunk *Region::at_location(int32_t x, int32_t z){
        for(size_t i = 0; i < chunks.size(); i++){
            if(chunks[i].x_pos == x && chunks[i].z_pos == z) return &chunks[i];
        }
        return &chunks.back();
    }
}